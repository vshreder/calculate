
$(window).click(function() {
	if ($('.student-start-lecture-modal').css('display') === 'block'){
		$('.student-start-lecture-modal').css('display', 'none');
	}
	if ($('.student-hamburger-menu').css('display') === 'block'){
		$('.student__wrap').css('display', 'none');
	}
	modalStock.forEach(function(model){
		if ($(model.name).css('display') === 'block'){
			$(model.name).css('display', 'none');
		}
	});
	$('.wrap').removeClass('faded-screen');
});

$('#student-start-lecture').click(function(){
	$('.student-start-lecture-modal').css('display', 'block');
	$('.wrap').addClass('faded-screen');
});

$('#student-hamburger-menu').click(function(){
	var studentMenu = $('.student__wrap').css('display');
	$('.student__wrap').css('display', studentMenu === 'block' ? 'none' : 'block');
});

$('#student-start-lecture-modal-close').click(function(){
	$('.wrap').removeClass('faded-screen');
	$('.student-start-lecture-modal').css('display', 'none');
});

modalStock = [
	{
		open: '.js-button-campaign7',
		close: '.js-close-campaign7',
		name: '.js-overlay-campaign7'
	},
	{
		open: '.js-button-campaign1',
		close: '.js-close-campaign1',
		name: '.js-overlay-campaign1'
	},
	{
		open: '.js-button-campaign2',
		close: '.js-close-campaign2',
		name: '.js-overlay-campaign2'
	},
	{
		open: '.js-button-campaign3',
		close: '.js-close-campaign3',
		name: '.js-overlay-campaign3'
	},
	{
		open: '.js-button-campaign4',
		close: '.js-close-campaign4',
		name: '.js-overlay-campaign4'
	}

];

function findProperModal(classNamesArray){
	var i = 0;
	while(i < classNamesArray.length){
		var curModal = modalStock.find(function(modal) {
			return (modal.open === '.' + classNamesArray[i] ||
				modal.close === '.' + classNamesArray[i] ||
				modal.name === '.' + classNamesArray[i]);
		});
		if (curModal){
			return curModal;
		}
		i++;
	}
}

function closeModal(event){
	var currentClass = findProperModal(event.target.classList);
	console.log('result', currentClass);
	$('.wrap').removeClass('faded-screen');
	$(currentClass.name).css('display', 'none');
}

function openModal(event) {
	console.log('open modal this.className', event.target.classList)
	var currentClass = findProperModal(event.target.classList);
	$('.wrap').addClass('faded-screen');
	$(currentClass.name).css('display', 'block');
}

$('#student-hamburger-menu').click(function(event){
	event.stopPropagation();
});

modalStock.forEach(function(model){
	$(model.close).click(function(event){
		closeModal(event);
	});
	$(model.open).click(function(event){
		openModal(event);
	});
	$(model.open).click(function(event){
		event.stopPropagation();
	});
	$(model.name).click(function(event){
		event.stopPropagation();
	});
});

$('.student__wrap').click(function(event){
	event.stopPropagation();
});

$('#student-start-lecture').click(function(event){
	event.stopPropagation();
});

$('.student-start-lecture-modal').click(function(event){
	event.stopPropagation();
});

function createSaveSettingsDiv (inputName){
	const saveSettingsDiv = document.createElement('div');
	var innerText;
	switch (inputName)  {
		case 'new-email':
			innerText = 'e-mail';
			break ;
		case 'new-pass':
			innerText = 'пароль';
			break ;
		default:
			innerText = 'изменения';
	}
	saveSettingsDiv.setAttribute('class', 'settings-input-save');
	saveSettingsDiv.innerHTML = "<button class='student__content__button'>Сохранить " + innerText;
	saveSettingsDiv.innerHTML += "</button><p>Отмена</p>";
	return saveSettingsDiv;
}

$('.student__content__settings__info__basic input').on('input', function (event){
	var inputVal;
	$( this ).val(function( i, val ) {
		inputVal = val;
		return val;
	  });
	const currentInput = event.currentTarget;
	const settingsInputLabelWrap = currentInput.parentNode;
	const inputName = currentInput.name;
	if (!inputVal && settingsInputLabelWrap.querySelector('.settings-input-save')){
		return settingsInputLabelWrap.querySelector('.settings-input-save').remove();
	}
	if (!settingsInputLabelWrap.querySelector('.settings-input-save')){
		return event.currentTarget.parentNode.appendChild(createSaveSettingsDiv(inputName));
	}
});


$('.student__coupons__inner__title').click (function() {
	const display_act = $('.student__coupons__wrap_activated').css('display');
	const display_deact = $('.student__coupons__wrap_deactivated').css('display');
	$('.student__coupons__wrap_activated').css('display', display_act === 'block' ? 'none' : 'block');
	$('.student__coupons__wrap_deactivated').css('display', display_deact === 'block' ? 'none' : 'block');
});
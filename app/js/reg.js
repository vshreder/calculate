$('.student__content__inner__title').click (function() {
	const display_act = $('.create-account').css('display');
	const display_deact = $('.add-student').css('display');
	$('.create-account').css('display', display_act === 'block' ? 'none' : 'block');
	$('.add-student').css('display', display_deact === 'block' ? 'none' : 'block');
	$('.students-wrap').addClass('faded-screen');
});